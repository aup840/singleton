package singleton;

public class SingletonExercise {
	  public static void main(String args[]) {
		    Deck deck = Deck.getInstance();
		    deck.print( );
		    Deck deck2 = Deck.getInstance();
		  }
}
