package singleton;
import java.util.*;

class Deck {
	private List<Card> cards;
	private volatile static Deck uniqueInstance;
	
	public static Deck getInstance(){
		if (uniqueInstance == null) {
			synchronized (Deck.class){
				if (uniqueInstance == null) {
					System.out.println("Creating unique instance of Deck");
					uniqueInstance = new Deck();
				}				
			}
		}
		else{
			System.out.println("Returning instance of Deck");
		}
		
		return uniqueInstance;
	
	}
	
	
	
	public Deck( ) {
		cards = new ArrayList<Card>( );	
	    // build the deck
	    Suit[] suits = {Suit.SPADES, Suit.HEARTS, Suit.CLUBS, Suit.DIAMONDS};
	    for(Suit suit: suits) {
	    	for(int i = 2; i <= 14; i++) {
	    		cards.add(new Card(suit, i));
	    	}
	    }
	    Collections.shuffle(cards, new Random( ));     // shuffle it!
	}

	public void print( ) {
		for(Card card: cards) {
			card.print( );
		}
	}
}