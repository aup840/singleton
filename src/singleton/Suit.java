package singleton;

public enum Suit {
	  SPADES,
	  HEARTS,
	  CLUBS,
	  DIAMONDS
}